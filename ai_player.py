import random
import time
import sys
from copy import deepcopy


MAX_OP = 'max'
MIN_OP = 'min'
MAX_DEPTH = 2


class minimax_node:
	def __init__(self, optype, board, move):
		self.optype = optype
		self.children = []
		self.board = board
		self.alpha = -sys.maxsize
		self.beta = sys.maxsize
		self.val = None
		self.move = move


class AIPlayer:

	def __init__(self, name, color, board_size):
		self.type = "AI"
		self.name = name
		self.color = color
		self.board_size = board_size
		self.max_depth = MAX_DEPTH

	def printState(self, board):
	
		for i in range(self.board_size-2, -1, -1):
			print("\t", end="")
			for j in range(self.board_size):
				print("| " + str(board[i][j]), end=" ")
			print("|")
		print("\t  ", end="")
		for _ in range(self.board_size):
			print("_", end="   ")

		print("\n\t  ", end="")
		for i in range(self.board_size):
			print(i+1, end= (4 - len(str(i+1))) * " ")
		print("\n")





	def checkForNs(self, board):
		my_4count = 0
		my_3count = 0
		op_4count = 0
		op_3count = 0

		for i in range(self.board_size-1):
			for j in range(self.board_size):
				if board[i][j] == 'o':
					res = self.verticalCheck(board, i, j)
					if (res == 3):
						op_3count += 1
					elif (res == 4):
						op_4count += 1

					res = self.horizontalCheck(board, i, j)
					if (res == 3):
						op_3count += 1
					elif (res == 4):
						op_4count += 1

					threes, fours = self.diagonalCheck(board, i, j)
					op_3count += threes
					op_4count += fours

				if board[i][j] == 'x':
					res = self.verticalCheck(board, i, j)
					if (res == 3):
						my_3count += 1
					elif (res == 4):
						my_4count += 1

					res = self.horizontalCheck(board, i, j)
					if (res == 3):
						my_3count += 1
					elif (res == 4):
						my_4count += 1

					threes, fours = self.diagonalCheck(board, i, j)
					my_3count += threes
					my_4count += fours
		return (my_3count - op_3count), (my_4count - op_4count)



	def verticalCheck(self, board, row, col):
		consecutiveCount = 0

		for i in range(row, self.board_size-1):
			if board[i][col].lower() == board[row][col].lower():
				consecutiveCount += 1
			else:
				break

		if consecutiveCount == 3:
			return 3
		elif consecutiveCount >= 4:
			return 4

		return 0


	def horizontalCheck(self, board, row, col):
		consecutiveCount = 0

		for j in range(col, self.board_size):
			if board[row][j].lower() == board[row][col].lower():
				consecutiveCount += 1
			else:
				break

		if consecutiveCount == 3:
			return 3
		elif consecutiveCount >= 4:
			return 4
		return 0

	def diagonalCheck(self, board, row, col):
		_4count = 0
		_3count = 0

		# check for diagonals with positive slope
		consecutiveCount = 0
		j = col
		for i in range(row, self.board_size-1):
			if j > self.board_size-1:
				break
			elif board[i][j].lower() == board[row][col].lower():
				consecutiveCount += 1
			else:
				break
			j += 1

		if consecutiveCount == 3:
			_3count += 1
		elif consecutiveCount >= 4:
			_4count += 1

		# check for diagonals with negative slope
		consecutiveCount = 0
		j = col
		for i in range(row, -1, -1):
			if j > self.board_size-1:
				break
			elif board[i][j].lower() == board[row][col].lower():
				consecutiveCount += 1
			else:
				break
			j += 1 # increment column when row s decremented

		if consecutiveCount == 3:
			_3count += 1
		elif consecutiveCount >= 4:
			_4count += 1

		return _3count, _4count





	def make_move(self, board, move, optype): #out of range possibility
		if optype == MAX_OP:
			color = 'x'
		else:
			color = 'o'
		for i in range(self.board_size-1):
			if board[i][move] == ' ':
				board[i][move] = color
				return True
		return False


	def getNextOpType(self, op):
		if(op == MAX_OP):
			return MIN_OP
		return MAX_OP


	def calculate_val(self, node):
		_3count, _4count = self.checkForNs(node.board)
		return (10 * _4count) + (3 * _3count)


	def build_minimax_tree(self, root, depth):

		# print('************** in depth', depth, '*****************')
		# self.printState(root.board)

		if (depth == 2 * self.max_depth):
			root.val = self.calculate_val(root)
			# print('leaf value ~~~~~~~> ', root.val)
			return root.val

		else:
			for i in range(self.board_size):
				current_board = deepcopy(root.board)
				if self.make_move(current_board, i, root.optype):
					nextOp = self.getNextOpType(root.optype)
					new_child = minimax_node(nextOp, current_board, i)
					root.children.append(new_child)
					score = self.build_minimax_tree(new_child, depth + 1)
					

					if root.optype == MIN_OP:
						if score < root.beta:
							root.beta = score
						if score < root.alpha:
							break

					if root.optype == MAX_OP:
						if score > root.alpha:
							root.alpha = score
						if score > root.beta:
							break


			score = None
			if root.optype == MIN_OP:
				score = self.get_min_val(root.children)
				# print ('in min op : val=', score)
				if score > root.alpha:
					root.alpha = score
					root.val = score

			if root.optype == MAX_OP:
				score = self.get_max_val(root.children)
				# print ('in max op : val=', score)
				if score < root.beta:
					root.beta = score
					root.val = score

			if depth == 0:
				return self.get_max_arg(root.children)
			return score


	def get_min_val(self, children):
		min_val = sys.maxsize
		for node in children:
			if node.val < min_val:
				min_val = node.val
		return min_val

	def get_max_arg(self, children):
		max_val = -sys.maxsize
		max_node = None
		for node in children:
			if node.val > max_val:
				max_val = node.val
				max_node = node
		return max_node


	def get_max_val(self, children):
		max_val = -sys.maxsize
		for node in children:
			if node.val > max_val:
				max_val = node.val
		return max_val



	def move(self, state):
		print("{0}'s turn.  {0} is {1}".format(self.name, self.color))
		choice_node = self.build_minimax_tree(minimax_node(MAX_OP, state, None), 0)
		return choice_node.move

